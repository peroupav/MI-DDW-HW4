import csv
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import numpy as np

with open('data/casts.csv', mode='r') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='\"')

    G = nx.Graph()

    # declare dictionary for actors and their ids and id_inc for assigning ids
    dict = {}
    id_inc = 0

    # variable for identifying row ranges of one movie
    movcode = ""
    # actors of one movie
    actors_of_movie = []

    # iterate over roles and create graph, nodes for actors and edges for those who acted in same move
    for row in reader:
        if movcode == "":
            movcode = row[0]
        if movcode != row[0]:
            # creation of edges between actors of movie
            for i in range(0, len(actors_of_movie)):
                for j in range(i + 1, len(actors_of_movie)):
                    G.add_edge(actors_of_movie[i], actors_of_movie[j])

            movcode = row[0]
            actors_of_movie = []

        actor = row[2]
        actor_id = None
        # add actor to dictionary
        if actor not in dict:
            actor_id = id_inc
            dict[actor] = actor_id
            id_inc += 1
        if actor_id is None:
            actor_id = dict[actor]

        # add actor_id to collection of current movie
        actors_of_movie.append(actor_id)
        # add actor node to grapg
        G.add_node(actor_id)

        # if row[0] == "AAn12":
        #     break

    # creation of edges between actors of movie for last movie outside for loop
    for i in range(0, len(actors_of_movie)):
        for j in range(i + 1, len(actors_of_movie)):
            G.add_edge(actors_of_movie[i], actors_of_movie[j])

    # inverse dictionary of names of actors by id
    inverse_dict = {v:k for k, v in dict.items()}

    # general statistic
    number_of_nodes = G.number_of_nodes()
    number_of_edges = G.number_of_edges()
    number_of_possilbe_edges = number_of_nodes * (number_of_nodes - 1) / 2
    density = number_of_edges / number_of_possilbe_edges
    number_of_components = len([component for component in nx.connected_components(G)])

    print("========= GENERAL STATISTIC =========")
    print("number of nodes: {}".format(number_of_nodes))
    print("number of edges: {}".format(number_of_edges))
    print("density: {:.5%}".format(density))
    print("number of components: {}".format(number_of_components))

    # number of top centrality nodes
    TOP_N = 5
    # top centrality actors
    key_players = set()
    # compute centralities by different measures
    for centrality in ["degree", "closeness", "betweenness", "eigenvector"]:
        if centrality == "degree":
            centrality_nodes = nx.degree_centrality(G)
        elif centrality == "closeness":
            centrality_nodes = nx.closeness_centrality(G)
        elif centrality == "betweenness":
            centrality_nodes = nx.betweenness_centrality(G)
        else:
            centrality_nodes = nx.eigenvector_centrality(G)

        # get top N centralities
        argsorted = np.argsort([centrality for _, centrality in centrality_nodes.items()])[::-1]
        print("========= TOP {} NODE BY {} CENTRALITY =========".format(TOP_N, centrality.upper()))
        for q in range(TOP_N):
            print("{}\t\t{:.5}".format(inverse_dict[argsorted[q]], centrality_nodes[argsorted[q]]))
            key_players.add(argsorted[q])

    # calculate average Kevin Bacon numbers of all actors with top N centrality actors
    # (ignoring nodes from different components where path doesn't exist)
    print("========= AVERAGE KEVIN BACON NUMBERS FOR TOP CENTRALITY ACTORS =========")
    for key_player in key_players:
        sum = 0
        cnt = 0
        for i, node in enumerate(G.nodes()):
            try:
                path_len = len(nx.shortest_path(G, source=node, target=key_player))
                sum += path_len
                cnt += 1
            except nx.exception.NetworkXNoPath:
                pass
        print("{}\t\t{:.5}".format(inverse_dict[key_player], sum/cnt))

    print("========= CLIEUQES =========")
    # set of actors of movies casted by five actors (or other cluqies of size 5)
    five_actor_movies_actors = set()
    for clique in nx.find_cliques(G):
        if len(clique) == 5:
            five_actor_movies_actors = five_actor_movies_actors.union(clique)

    # create new graph occupied only by cliques of size 5
    G5 = nx.Graph()
    G5.add_nodes_from(five_actor_movies_actors)

    for edge in G.edges_iter(five_actor_movies_actors):
        if edge[0] in five_actor_movies_actors and edge[1] in five_actor_movies_actors:
            G5.add_edge(edge[0], edge[1])

    print("number of nodes: {}".format(G5.number_of_nodes()))
    print("number of edges: {}".format(G5.number_of_edges()))

    # visualise
    plt.figure(figsize=(20, 10))
    pos = graphviz_layout(G5, prog="fdp")
    nx.draw(G5, pos,
            labels=inverse_dict,
            cmap=plt.get_cmap("bwr"),
            node_color=[G5.degree(v) for v in G5],
            font_size=12
            )
    plt.show()
    